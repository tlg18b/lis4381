import java.util.Scanner;

public class Methods {
   public static void getRequirements() {

    System.out.println("Developer: Tracy Gaboyau");
    System.out.println("Program evaluates largest of two integers.");
    System.out.println("Note: Program does *not* check for non-numeric characters or non-integer values.");
    System.out.println();

   } 

   public static void largestNumber() {

    Scanner reader = new Scanner(System.in);
    System.out.println("Enter first integer:");
    int num = reader.nextInt();

    Scanner readertwo = new Scanner(System.in);
    System.out.println("Enter second integer:");
    int numtwo = readertwo.nextInt();

    if (num > numtwo)
        System.out.println(num + " is larger than " + numtwo);
    if (num < numtwo)
        System.out.println(numtwo + " is larger than " +num);
    else
        System.out.println("Integers are equal.");

    /* int num1, num2;
    input = new Scanner(System.in);

    System.out.print("Enter first integer: ");
    num1 = input.nextint();

    System.out.print("Enter second integer: ");
    num2 = input.nextint();

    System.out.println();

    if (num1 > num2)
    System.out.println(num1 + "is larger than" + num2);
    else if (num2 > num1)
    System.out.println(num2 + "is larger than" + num1);
    else 
    System.out.println("Integers are equal."); */

   }

   /* public static int getNum() {
       Scanner sc = new Scanner(System.in);
       return sc.nextInt(); */
   
}

