> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Tracy Gaboyau

### Assignment 2 Requirements:

*Three Parts*

1. Create a mobile recipe app using Android Studio
2. Complete skillsets
3. PHP/MySQL: Chs. 3, 4)

#### README.md file should include the following items:

* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface


#### Assignment Screenshots:

*Screenshot of running application’s first user interface*:

![First User Interface](img/first.png)

*Screenshot of running application’s second user interface*:

![Second User Interface](img/second.png)



#### Skillset Screenshots:

*Screenshot SS1 running*:

![SS1](img/evenorodd.png)

*Screenshot SS2 running*:

![SS2](img/largest.png)

*Screenshot SS3 running*:

![SS3](img/arraysandloops.png)

