import java.util.Scanner;

public class Methods {
    public static void getRequirements() {

    System.out.println("Developer: Tracy Gaboyau");
    System.out.println("Program evaluates integers as even or odd.");
    System.out.println("Note: Program does *not* check for non-numeric characters.");
    System.out.println();
    }

    public static void evaluateNumber()
    {
   
    Scanner reader = new Scanner(System.in);
    System.out.println("Enter an Integer number:");
    int num = reader.nextInt();

    if (num % 2 == 0)
        System.out.println(num + " is an even  number.");
    else
        System.out.println(num + " is an odd number.");

    }

    }
