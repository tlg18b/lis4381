<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="A showcase of my skills and abilities acquired while working through various project requirements for LIS4381.">
		<meta name="author" content="Tracy Gaboyau">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
				<strong>Assignment 2 Requirements:</strong> 

<ol class="text-justify">
  <li>Create a mobile recipe app using Android Studio</li>
  <li>Complete skillsets</li>
  <li>PHP/MySQL: Chs. 3, 4)</li>
</ol>

<strong>README.md file should include the following items:</strong>

<ul class="text-justify">
  <li>Screenshot of running application’s first user interface</li>
  <li>Screenshot of running application’s second user interface</li>
</ul>
				</p>

				<h4>Screenshot of running application’s first user interface:</h4>
				<img src="img/first.png" class="img-responsive center-block" alt="*Screenshot of running application’s first user interface*:"
>

				<h4>*Screenshot of running application’s second user interface*:</h4>
				<img src="img/second.png" class="img-responsive center-block" alt="*Screenshot of running application’s second user interface*:
">

				<h4>Skillset Screenshots</h4>
				<img src="img/evenorodd.png" class="img-responsive center-block" alt="1">
				<img src="img/largest.png" class="img-responsive center-block" alt="2">
				<img src="img/arraysandloops.png" class="img-responsive center-block" alt="3">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
