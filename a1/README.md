> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Tracy Gaboyau

### Assignment 1 Requirements:

*Three Parts*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1, 2)

#### README.md file should include the following items:

* Screenshot of ampps installation running
* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App
* git commands w/short descriptions
* Bitbucket repo links

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - this creates a new git repository
2. git status - this displays the state of your directory i.e if files are staged or not, if you have any changed files in your directory that have not been commited
3. git add - adds new or changed files in your working directory to git staging area
4. git commit - creates a snapshot of your entire repository at specific times
5. git push - updates the remote branch with local commits
6. git pull - updates your current local working branch, and all of the remote tracking branches
7. git remote -v - lists remote repositories from current remote connections

#### Assignment Screenshots:

*Screenshot of AMPPS running:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*: https://ibb.co/xhGPZ1L

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*: https://ibb.co/grr82c0

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/tlg18b/bitbucketstationlocations/ "Bitbucket Station Locations")

