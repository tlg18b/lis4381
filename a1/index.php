<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="A showcase of my skills and abilities acquired while working through various project requirements for LIS4381.">
		<meta name="author" content="Tracy Gaboyau">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Assignment 1 Requirements::</strong> 

<ol class="text-justify">
  <li>Distributed Version Control with Git and Bitbucket</li>
  <li>Development Installations</li>
  <li>Chapter Questions (Chs 1, 2)</li>
</ol>

README.md file should include the following items:

<ul class="text-justify">
  <li>Screenshot of ampps installation running</li>
  <li>Screenshot of running JDK java Hello</li>
  <li>Screenshot of running Android Studio My First App</li>
  <li>git commands w/ short descriptions</li>
  <li>Bitbucket repo links</li>
</ul>
				</p>

				<h4>Java Installation</h4>
				<img src="img/jdk_install.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio Installation</h4>
				<img src="img/android.png" class="img-responsive center-block" alt="Android Studio Installation">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
