-- MySQL Script generated by MySQL Workbench
-- Thu Oct  7 11:33:47 2021
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema tlg18b
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `tlg18b` ;

-- -----------------------------------------------------
-- Schema tlg18b
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tlg18b` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
SHOW WARNINGS;
USE `tlg18b` ;

-- -----------------------------------------------------
-- Table `tlg18b`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tlg18b`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tlg18b`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` CHAR(5) NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) NOT NULL,
  `cus_total_sales` DECIMAL(8,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tlg18b`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tlg18b`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tlg18b`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` CHAR(5) NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `tlg18b`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `tlg18b`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `tlg18b`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pets_customer_idx` (`cus_id` ASC) VISIBLE,
  INDEX `fk_pets_petstore1_idx` (`pst_id` ASC) VISIBLE,
  CONSTRAINT `fk_pets_customer`
    FOREIGN KEY (`cus_id`)
    REFERENCES `tlg18b`.`customer` (`cus_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pets_petstore1`
    FOREIGN KEY (`pst_id`)
    REFERENCES `tlg18b`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `tlg18b`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `tlg18b`;
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Bob', 'Saget', '739 Crazy Way', 'Lake City', 'FL', '87908', 9384038710, 'bob@gmail.com', 392.00, 2910.20, 'notes1');
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Susan', 'Boyle', '584 Nice Lane', 'Winter Park', 'FL', '78092', 4736920934, 'susan@aol.com', 374.39, 3948.19, 'notes2');
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Maggie', 'Wond', '583 Cute Dr.', 'Sarasota', 'FL', '72639', 6758493029, 'maggie@outlook.com', 43.04, 139.20, 'notes3');
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Drake', 'Grahm', '483 Green St.', 'Jacksonville', 'FL', '17263', 3748392839, 'drake@gmail.com', 32.04, 338.40, 'notes4');
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Ariana', 'Grande', '473 Blue Dr.', 'Orlando', 'FL', '29384', 4380929403, 'ariani@gmail.com', 382.50, 506.54, 'notes5');
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Solana', 'Muse', '483 Drum Blvd.', 'Tampa', 'FL', '48273', 3948392039, 'solanna@aol.com', 1.23, 302.34, 'notes6');
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Ryan', 'Destiny', '203 Call St.', 'Miami', 'FL', '39204', 4958392049, 'ryan@yahoo.com', 323.34, 593.34, 'notes7');
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Keith', 'Powers', '574 Primrose Dr.', 'Boca Raton', 'FL', '40503', 3948392049, 'keith@gmail.com', 5.65, 32.34, 'notes8');
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Keke', 'Palmer', '685 Cullen Way', 'West Palm Beach', 'FL', '39405', 4839748573, 'keke@outlook.com', 403.23, 596.43, 'notes9');
INSERT INTO `tlg18b`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Zendaya', 'Coleman', '523 Right Way', 'Key West', 'FL', '20394', 3727485749, 'zenny@gmail.com', 230.44, 543.21, 'notes10');

COMMIT;


-- -----------------------------------------------------
-- Data for table `tlg18b`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `tlg18b`;
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Furry Frenzy ', '232 Sharm Rd.', 'FL', '32649', 4637283940, 'furryfrenzy@gmail.com', 'www.furryfriends.com', 5004, 'note1', 'Tallahassee');
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Best Pets', '748 Rock St.', 'FL', '32094', 5738493018, 'bestpets@aol.com', 'www.bestpets.com', 5906, 'note2', 'Sarasota');
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Pet Palace', '429 Shimmy Blvd.', 'FL', '32288', 3678927405, 'petpalace@yahoo.com', 'www. petpalace.com', 3940, 'note3', 'Orlando');
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Claws and Paws', '746 University Rd.', 'FL', '34060', 4739472918, 'clawsnpaws@gmail.com', 'www.clawsandpaws.com', 4905, 'note4', 'Miami');
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Purrs and Grrs', '684 Riverton Rd.', 'FL', '34960', 5673981732, 'prrsngrrs@gmail.com', 'www.prrsandgrss.com', 5860, 'note5', 'Winter Park');
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Pet Partners', '690 Leila St.', 'FL', '32918', 7469304638, 'petpartners@yahoo.com', 'www.petpartners.com', 4930, 'note6', 'Boca Raton');
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Furry Friends', '573 Belle Rd.', 'FL', '28405', 5738209485, 'furryfriends@aol.com', 'www.furryfriends.com', 5940, 'note7', 'Jacksonville');
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Cute Creatures', '793 Ravioli Way', 'FL', '37269', 7241937509, 'cutecreatures@outlook.com', 'www.cutecreatures.com', 4950, 'note8', 'Tampa');
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Fun Flock', '583 Doowin Rd.', 'FL', '38495', 6142974903, 'funflock@yahoo.com', 'www.funflock.com', 6070, 'note9', 'St. Petersburg');
INSERT INTO `tlg18b`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`, `pst_city`) VALUES (DEFAULT, 'Pawz', '473 MLK Dr.', 'FL', '69473', 1638940967, 'pawz@gmail.com', 'www.pawz.com', 7069, 'note10', 'Lake City');

COMMIT;


-- -----------------------------------------------------
-- Data for table `tlg18b`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `tlg18b`;
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 4, 'Pitbull', 'f', 403, 678, 3, 'black', '2008-08-07', 'y', 'y', 'notes1');
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 2, 'Cat', 'm', 402, 987, 6, 'white', '2010-08-09', 'y', 'y', 'notes2');
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 3, 'Rabbit', 'f', 506, 435, 3, 'brown', '2009-09-08', 'y', 'n', 'notes3');
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 4, 'Hamster', 'f', 192, 876, 5, 'green', '2008-09-07', 'n', 'n', 'notes4');
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 5, 'Lizard', 'm', 394, 654, 5, 'blue', '2010-06-04', 'n', 'n', 'notes5');
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 6, 'Parakeet', 'm', 291, 765, 6, 'red', '2014-09-04', 'y', 'y', 'notes6');
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 7, 'Gopher', 'f', 192, 432, 3, 'purple', '2006-09-05', 'n', 'y', 'notes7');
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 4, 'Poodle', 'm', 394, 876, 4, 'green', '2007-09-06', 'n', 'n', 'notes8');
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 3, 'Cheetah', 'm', 293, 543, 7, 'yellow', '2008-06-04', 'y', 'y', 'notes9');
INSERT INTO `tlg18b`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 2, 'Lion', 'f', 493, 897, 4, 'brown', '2003-09-05', 'y', 'n', 'notes10');

COMMIT;

