> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Tracy Gaboyau

### Assignment 3 Requirements:

*Sub-Heading:*

1. Create a mobile ticket value app   
2. Create a database in MySQL with 3 tables and 10 records in each table 
3. Complete skillsets
4. Chapter Questions Chs. 5, 6

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface
* Screenshots of 10 records for each table
* Links to a3.mwb & a3.sql


#### Assignment Screenshots:

*Screenshot ERD*:

![ERD Screenshot](img/ERD.png)

*Screenshot of running application’s first user interface*:

![running application’s first user interface](img/first.png)

*Screenshot of running application’s second user interface*:

![running application’s second user interface](img/second.png)

*Screenshot of 10 records for each table*:

*pet table*:

![10 records for pet table](img/pets.png)

*customer table*:

![10 records for customer table](img/customer.png)

*petstore table*:

![10 records for petstore table](img/petstore.png)


# Links to a3.mwb & a3.sql

*Password: vandel123 (still trying to figure out how to change it from previous class/download)*

[a3.mwb](docs/a3.mwb)

[a3.sql](docs/a3.sql)

# Skillset Screenshots

*ss4*:

![ss4](img/ss4.png)

*ss5*:

![ss5](img/ss5.png)

*ss6*:

![ss6](img/ss6.png)
