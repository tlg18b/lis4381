> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Tracy Gaboyau

### Assignment 4 Requirements:

*Sub-Heading:*

1. cd to local repos subdirectory:
2. Clone assignment  starter files
3. Review subdirectories and files
4. Review index.php code
5. Create favicon with my initials

#### README.md file should include the following items:

* Screenshot of LIS4381 Portal (Main Page) 
* Screenshot of Failed Validation
* Screenshot of Passed Validation 
* Link to local lis4381 web app


#### Assignment Screenshots:

*Screenshot of LIS4381 Portal (Main Page)*:

![Screenshot of LIS4381 Portal (Main Page)](img/portal.png)

*Screenshot of Failed Validation*:

![Screenshot of Failed Validation](img/failed.png)

*Screenshot of Passed Validation*:

![Screenshot of Passed Validation](img/passed.png)

# Link to local web app: 

http://localhost:8080/index.php

# Skillset Screenshots

*ss10*:

![ss10](img/ss10.png)

*ss11*:

![ss11](img/ss11.png)

*ss12*:

![ss12](img/ss12.png)
