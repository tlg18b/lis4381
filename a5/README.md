> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Tracy Gaboyau

### Assignment 5 Requirements:

1. Basic server-side Validation
2. Display user-entered data
3. Permit users to add data
4. Assigned Skillsets
5. Connect SQL db to php

#### README.md file should include the following items:

* Screenshot of index.php 
* Screenshot of add_petstore.php (invalid) 
* Screenshot of add_petstore_process.php (Failed Validation)  
* Screenshot of add_petstore.php (valid) 
* Screenshot of add_petstore_process.php (Passed Validation) 


#### Assignment Screenshots:

*Screenshot of index.php*:

![Screenshot of index](img/index.png)

*Screenshot of add_petstore.php (invalid)*:

![Screenshot of add_petstore.php (invalid) ](img/invalid.png)

*Screenshot of add_petstore_process.php (Failed Validation)*:

![Screenshot of add_petstore_process.php (Failed Validation) ](img/failed.png)

*Screenshot of add_petstore.php (valid)*:

![Screenshot of add_petstore.php (valid) ](img/valid.png)

*Screenshot of add_petstore_process.php (Passed Validation)*:

![Screenshot of add_petstore_process.php (Passed Validation)](img/passed.png)

# Link to local web app: 

http://localhost:8080/index.php

# Skillset Screenshots

*ss13*:

![ss13](img/ss13.png)

*ss14*:

![ss14](img/ss14a.png)
![ss14](img/ss14b.png)
![ss14](img/ss14c.png)
![ss14](img/ss14d.png)

*ss15*:

![ss15](img/ss15a.png)
![ss15](img/ss15b.png)