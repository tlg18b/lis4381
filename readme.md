> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Tracy Gaboyau

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md ](https://bitbucket.org/tlg18b/lis4381/src/master/a1/README.md "A1 Readme.md")
    * Distributed Version Control with Git and Bitbucket
    * Development Installations
    * Chapter Questions (Chs 1, 2)

2. [A2 README.md ](https://bitbucket.org/tlg18b/lis4381/src/master/a2/README.md "A2 Readme.md")
    * Create a mobile recipe app using Android Studio
    * Complete skillsets
    * PHP/MySQL: Chs. 3, 4
    
3. [A3 README.md ](https://bitbucket.org/tlg18b/lis4381/src/master/a3/README.md "A3 Readme.md")
    * Create a mobile ticket value app   
    * Create a database in MySQL with 3 tables and 10 records in each table 
    * Complete skillsets
    * Chapter Questions Chs. 5, 6

4. [P1 README.md ](https://bitbucket.org/tlg18b/lis4381/src/master/p1/README.md "P1 Readme.md")
    * Create a launcher icon image and display it in both activities (screens)
    * Must add background color(s) to both activities
    * Must add border around image and button
    * Must add text shadow (button)

5. [A4 README.md ](https://bitbucket.org/tlg18b/lis4381/src/master/a4/README.md "A4 Readme.md")
    * cd to local repos subdirectory:
    * Clone assignment  starter files
    * Review subdirectories and files
    * Review index.php code
    * Create favicon with my initials

6. [A5 README.md ](https://bitbucket.org/tlg18b/lis4381/src/master/a5/README.md "A5 Readme.md")
    * Basic server-side Validation
    * Display user-entered data
    * Permit users to add data
    * Assigned Skillsets
    * Connect SQL db to php

7. [P2 README.md ](https://bitbucket.org/tlg18b/lis4381/src/master/p2/README.md "P2 Readme.md")
    * Display proper titles, nav links, headers, etc.
    * Turn off client-side validation 
    * Add server-side validation and regular expressions
    * Provide proper screenshots
    