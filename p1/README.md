> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381

## Tracy Gaboyau

### Project 1 Requirements:

1. Create a launcher icon image and display it in both activities (screens)
2. Must add background color(s) to both activities
3. Must add border around image and button
4. Must add text shadow (button)

#### README.md file should include the following items:

* Coursetitle, your name, assignment requirements, as per A1
* Screenshot of running application’s first user interface
* Screenshot of running application’s second user interface


#### Assignment Screenshots:

*Screenshot of running application’s first user interface*:

![running application’s first user interface](img/first.png)

*Screenshot of running application’s second user interface*:

![running application’s second user interface](img/second.png)


## Skillset Screenshots

*Screenshot of ss7*:

![ss7](img/ss7.png)

*Screenshot of ss8*:

![ss8](img/ss8.png)

*Screenshot of ss9*:

![ss9](img/ss9.png)