> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381

## Tracy Gaboyau

### Project 2 Requirements:

1. Display proper titles, nav links, headers, etc.
2. Turn off client-side validation 
3. Add server-side validation and regular expressions
4. Provide proper screenshots

#### README.md file should include the following items:

* Screenshot of Carousel (Home page –include images, self-promotional links) 
* Screenshot of index.php 
* Screenshot of edit_petstore.php  
* Screenshot of Failed Validation 
* Screenshot of Passed Validation 
* Screenshot of Delete Record Prompt 
* Screenshot of Successfully Deleted Record
* Screenshot of RSS Feed


#### Assignment Screenshots:

*Screenshot of Carousel (Home page –include images, self-promotional links) *:

![Screenshot of Carousel](img/carousel.png)

*Screenshot of index.php *:

![Screenshot of index.php](img/index.png)

*Screenshot of edit_petstore.php *:

![Screenshot of edit_petstore.php](img/edit.png)

*Screenshot of Failed Validation *:

![Screenshot of Failed Validation ](img/failed.png)

*Screenshot of Passed Validation *:

![Screenshot of Passed Validation ](img/passed.png)

*Screenshot of Delete Record Prompt*:

![Screenshot of Delete Record Prompt](img/prompt.png)

*Screenshot of Successfully Deleted Record*:

![Screenshot of Successfully Deleted Record](img/delete.png)

*Screenshot of RSS Feed*:

![Screenshot of RSS Feed](img/rss.png)

# Link to local web app: 

http://localhost:8080/index.php
